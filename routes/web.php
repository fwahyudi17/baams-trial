<?php

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout', function () {
    Auth::logout();

    return view('auth.login');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::group(['prefix' => 'mgr', 'namespace' => 'Manager'], function () {
    Route::name('transaction-manager')->get('/getTransaction', 'ManagerController@getTransaction');
    Route::name('transactionTable.vue')->get('/getUsers', 'ManagerController@getUsers');
});
Route::group(['namespace' => 'Manager', 'prefix' => 'manager', 'middleware' => ['isManager', 'auth']], function () {
    Route::name('manager.index')->get('/', 'ManagerController@index');
    Route::name('manager.records')->get('/records', 'ManagerController@getRecords');
});

Route::group(['prefix' => 'user'], function () {
    Route::name('transaction-user')->get('/getTransaction', 'HomeController@getTransaction');
    Route::name('withdraw')->post('/withdraw', 'HomeController@withdraw');
    Route::name('deposit')->post('/deposit', 'HomeController@deposit');
    Route::name('updateBalance')->get('/balance', 'HomeController@updateBalance');
    Route::name('transfer')->post('/transfer', 'HomeController@transfer');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
