@extends('layouts.layout')

@section('content')
    <div class="wrapper ">
        @include('layouts.sidebar')
        <div class="main-panel">
            <div class="panel-header panel-header-sm">

            </div>
            <div class="content">
                <div class="row">
                    @foreach($accounts as $account)
                        <div class="col-lg-4">
                            <div class="card card-chart">
                                <div class="card-header">
                                    <h5 class="card-category">My Account</h5>
                                    <h4 class="card-title">#{{$account->account_number}}</h4>
                                    <div class="dropdown">
                                        <button type="button"
                                                class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret"
                                                data-toggle="dropdown">
                                            <i class="now-ui-icons loader_gear"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" onclick="withdraw({{$account->id}})">Withdraw</a>
                                            <a class="dropdown-item" onclick="deposit({{$account->id}})">Deposit</a>
                                            <a class="dropdown-item" onclick="transfer({{$account->id}}, {{$account->account_number}})">Transfer</a>
                                            <a class="dropdown-item"
                                               onclick="getTransaction({{$account->account_number}})" href="#">See
                                                Transaction</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body" id="balance">
                                    Rp.{{number_format($account->balance,2)}}
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <a onclick="updateBalance({{$account->id}})"><i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-lg-8" id="withdraw" style="display: none">
                        <div class="card card-chart">
                            <div class="card-header">
                                <h5 class="card-category">Withdraw</h5>
                            </div>

                            <div class="card-body">
                                <input class="form-control" type="number" name="withdraw_nominal" id="withdraw-nominal">
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary btn-block" id="submit-withdraw" type="submit">Withdraw</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8" id="deposit" style="display: none">
                        <div class="card card-chart">
                            <div class="card-header">
                                <h5 class="card-category">Deposit</h5>
                            </div>

                            <div class="card-body">
                                <input class="form-control" type="number" name="withdraw_nominal" id="deposit-nominal">
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary btn-block" id="submit-deposit" type="submit">Deposit</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8" id="transfer" style="display: none">
                        <div class="card card-chart">
                            <div class="card-header">
                                <h5 class="card-category">Transfer Out</h5>
                            </div>

                            <div class="card-body">
                                <label>Recipient Account Number</label>
                                <input class="form-control" type="number" name="recipient_id" id="recipient-acc">
                                <br>
                                <label>Transfer Nominal</label>
                                <input class="form-control" type="number" name="withdraw_nominal" id="transfer-nominal">
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary btn-block" id="submit-transfer" type="submit">Transfer</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"> My Transaction</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                        <th>
                                            No
                                        </th>
                                        <th>
                                            Account Number
                                        </th>
                                        <th>
                                            Customer Name
                                        </th>
                                        <th>
                                            Transaction Type
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        <th>
                                            Date
                                        </th>
                                        </thead>
                                        <tbody id="row-transaction">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <nav>
                        <ul>
                            <li>
                                <a href="https://www.creative-tim.com">
                                    Creative Tim
                                </a>
                            </li>
                            <li>
                                <a href="http://presentation.creative-tim.com">
                                    About Us
                                </a>
                            </li>
                            <li>
                                <a href="http://blog.creative-tim.com">
                                    Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright" id="copyright">
                        &copy;
                        <script>
                            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                        </script>
                        , Designed by
                        <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by
                        <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.
                    </div>
                </div>
            </footer>
        </div>
    </div>
@endsection
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">

    function withdraw(account) {
        $('#deposit').hide();
        $('#withdraw').show();
        $('#transfer').hide();
        $('#submit-withdraw').on('click', function(){
            $.ajax({
                url : '{{URL('/user/withdraw')}}',
                type : 'POST',
                data : {
                    _token : "{{ csrf_token() }}",
                    account_id : account,
                    nominal : $('#withdraw-nominal').val(),
                },
                success:function success(response){
                    $('#withdraw-nominal').val('');
                    showNotification(response.message);
                },
                error:function failed(response){
                    console.log(response);
                    showNotification(response.message);
                }
            });
            updateBalance(account);
        });
    }

    function deposit(account) {
        $('#deposit').show();
        $('#withdraw').hide();
        $('#transfer').hide();
        $('#submit-deposit').on('click', function(){
            $.ajax({
                url : '{{URL('/user/deposit')}}',
                type : 'POST',
                data : {
                    _token : "{{ csrf_token() }}",
                    account_id : account,
                    nominal : $('#deposit-nominal').val(),
                },
                success:function success(response){
                    $('#deposit-nominal').val('');
                    showNotification(response.message);
                },
                error:function failed(response){
                    console.log(response.message);
                }
            });
            updateBalance(account);
        });
    }

    function updateBalance(account){
        alert(account);
        $.ajax({
            url : '{{URL('/user/balance')}}',
            data : {
                account_id : account,
            },
            success:function success(response){
                console.log(response.balance);
                var balance = response.balance;
                $('#balance').html(balance);
                showNotification('Balance updated');
            },
            error:function failed(response){
                console.log(response.message);
            }
        })
    }

    function getTransaction(account) {
        $('#deposit').hide();
        $('#withdraw').hide();
        $.ajax({
            url: '{{URL('/user/getTransaction')}}',
            data: {
                'account_number': account
            },
            success: function success(data) {
                html = "";
                $('#row-transaction').html(html);
                var a = 1;
                for (i = 0; i <= data.data.length; i++) {
                    acc_number = data.data[i].account_number;
                    customer_name = data.data[i].name;
                    type = data.data[i].transaction_type;
                    amount = data.data[i].amount;
                    date = data.data[i].created_at;
                    html = "<tr><td>" + a++ + "</td><td>" + acc_number + "</td><td>" + customer_name + "</td><td>" + type + "</td><td>" + amount + "</td><td>" + date + "</td><tr>"
                    $('#row-transaction').append(html);
                }
            }
        });
    }

    function transfer(acc_id, acc_number){
        $('#deposit').hide();
        $('#withdraw').hide();
        $('#transfer').show();
        $('#submit-transfer').on('click', function() {
            $.ajax({
                url: '{{URL('/user/transfer')}}',
                type: 'POST',
                data: {
                    'sender_acc' : acc_number,
                    'recipient_acc' : $('#recipient-acc').val(),
                    'nominal' : $('#transfer-nominal').val(),
                    '_token':'{{csrf_token()}}'
                },
                success: function success(response) {
                    $('#recipient-acc').val('');
                    $('#transfer-nominal').val('');
                    updateBalance(acc_id);
                    showNotification(response.message);
                }
            })
        })
    }

    function showNotification(message) {
        color = 'primary';
        $.notify({
            icon: "now-ui-icons ui-1_bell-53",
            message: message,

        }, {
            type: color,
            timer: 4000,
            placement: {
                from: 'top',
                align: 'center'
            }
        });
    }
</script>