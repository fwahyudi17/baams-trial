@extends('layouts.layout')

@section('content')
    <div class="wrapper ">
        @include('layouts.sidebar')
        <div class="main-panel">
            <div class="panel-header panel-header-sm">
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card card-chart">
                            <div class="card-header">
                                <h5 class="card-category">Customer</h5>
                                <h4 class="card-title">Total Transaction</h4>
                                <div class="dropdown">
                                    <button type="button"
                                            class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret"
                                            data-toggle="dropdown">
                                        <i class="now-ui-icons loader_gear"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" onclick="getTransaction()">See All Transaction</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <a class="dropdown-item text-danger" href="#">Remove Data</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <h2>{{$transactions->count()}}</h2>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card card-chart">
                            <div class="card-header">
                                <h5 class="card-category">Customer</h5>
                                <h4 class="card-title">Total Customer</h4>
                                <div class="dropdown">
                                    <button type="button"
                                            class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret"
                                            data-toggle="dropdown">
                                        <i class="now-ui-icons loader_gear"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" onclick="getCustomers()">See All Customers</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <h2>{{$customers->count()}}</h2>
                            </div>
                            <div class="card-footer">
                                <div class="stats">
                                    <i class="now-ui-icons arrows-1_refresh-69"></i> Just Updated
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="display:none;" id="transaction-list">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"> Transactions Stats</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                        <th>
                                            No
                                        </th>
                                        <th>
                                            Account Number
                                        </th>
                                        <th>
                                            Customer Name
                                        </th>
                                        <th>
                                            Transaction Type
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        <th>
                                            Date
                                        </th>
                                        </thead>
                                        <tbody id="row-transaction">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="display:none;" id="customer-list">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"> Customer Stats</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                        <th>
                                            No
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Address
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Primary Account
                                        </th>
                                        <th>
                                            Joined at
                                        </th>
                                        </thead>
                                        <tbody id="row-customers">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <nav>
                        <ul>
                            <li>
                                <a href="https://www.creative-tim.com">
                                    Creative Tim
                                </a>
                            </li>
                            <li>
                                <a href="http://presentation.creative-tim.com">
                                    About Us
                                </a>
                            </li>
                            <li>
                                <a href="http://blog.creative-tim.com">
                                    Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright" id="copyright">
                        &copy;
                        <script>
                            document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
                        </script>
                        , Designed by
                        <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by
                        <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.
                    </div>
                </div>
            </footer>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function getTransaction() {
        $('#transaction-list').show();
        $('#customer-list').hide();
        $.ajax({
            url: '{{URL('/mgr/getTransaction')}}',
            success: function success(data) {
                html = "";
                $('#row-transaction').html(html);
                var a = 1;
                for (i = 0; i <= data.data.length; i++) {
                    console.log(data.data[i]);
                    acc_number = data.data[i].account_number;
                    customer_name = data.data[i].name;
                    type = data.data[i].transaction_type;
                    amount = data.data[i].amount;
                    date = data.data[i].created_at;
                    html = "<tr><td>" + a++ + "</td><td>" + acc_number + "</td><td>" + customer_name + "</td><td>" + type + "</td><td>" + amount + "</td><td>" + date + "</td><tr>"
                    $('#row-transaction').append(html);
                }
            }
        });
    }
    function getCustomers() {
        $('#transaction-list').hide();
        $('#customer-list').show();
        $.ajax({
            url: '{{URL('/mgr/getUsers')}}',
            success: function success(data) {
                html = "";
                $('#row-customers').html(html);
                var a = 1;
                for (i = 0; i <= data.data.length; i++) {
                    console.log(data.data[i]);
                    customer_name = data.data[i].customer_name;
                    address = data.data[i].address;
                    email = data.data[i].email;
                    primary_acc = data.data[i].primary_account;
                    date = data.data[i].joined_at;
                    html = "<tr><td>" + a++ + "</td><td>" + customer_name + "</td><td>" + address + "</td><td>" + email + "</td><td>" + primary_acc + "</td><td>" + date + "</td><tr>"
                    $('#row-customers').append(html);
                }
            }
        });
    }
</script>