<div class="sidebar" data-color="orange">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text logo-mini">
            CT
        </a>
        <a href="http://www.creative-tim.com" class="simple-text logo-normal">
            Hi, {{\Illuminate\Support\Facades\Auth::user()->first_name}} !!
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{\Illuminate\Support\Facades\Route::getCurrentRoute()->getName() == 'dashboard' ? 'active' : ''}}">
                <a href="{{route('dashboard')}}">
                    <i class="now-ui-icons design_app"></i>
                    <p>Dashboard | User</p>
                </a>
            </li>
                {{--Menu Untuk Manager--}}
            @if(\Illuminate\Support\Facades\Auth::user()->isManager == 1)
                <li class="{{\Illuminate\Support\Facades\Route::getCurrentRoute()->getName() == 'manager.index' ? 'active' : ''}}">
                    <a href="{{route('manager.index')}}">
                        <i class="now-ui-icons education_atom"></i>
                        <p>Dashboard | Manager</p>
                    </a>
                </li>
            @endif
            <li>
                <a href="{{route('logout')}}">
                    <i class="now-ui-icons location_map-big"></i>
                    <p>Sign-Out</p>
                </a>
            </li>
        </ul>
    </div>
</div>