<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['account_number', 'user_id' ,'balance'];
    protected $with = ['user'];

    public function user(){
        return $this->hasOne(User::class, 'id');
    }
}
