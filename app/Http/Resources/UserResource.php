<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'customer_name' => $this->first_name.' '.$this->last_name,
            'address' => $this->address,
            'email' => $this->email,
            'primary_account' => $this->accounts[0]->account_number,
            'joined_at' => Carbon::parse($this->created_at)->toDayDateTimeString(),
        ];
    }
}
