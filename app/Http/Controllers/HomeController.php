<?php

namespace App\Http\Controllers;

use App\Http\Resources\TransactionResource;
use App\Models\Account;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $accounts = Account::where('user_id', Auth::id())->get();
        $transactions = array();
        foreach ($accounts as $account) {
            $trans = Transaction::where('account_number', $account->account_number)->get();
            $transactions[] = $trans;
        }

        return view('dashboard', compact('accounts', 'transactions'));
    }

    public function getTransaction(Request $request)
    {
        $transactions = Transaction::where('account_number', $request->get('account_number'))->get();
        //return response()->json($transactions);
        return TransactionResource::collection($transactions);
    }

    public function withdraw(Request $request)
    {
        try {
            $account = Account::findOrFail($request->get('account_id'));
            $newBalance = $account->balance - $request->get('nominal');

            $a = Transaction::create([
                'account_number' => $account->account_number,
                'customer_name' => $account->user->first_name . ' ' . $account->user->last_name,
                'type' => 'withdraw',
                'amount' => $request->get('nominal')
            ]);
            $account->update([
                'balance' => $newBalance
            ]);
            return response()->json([
                'message' => 'Successfully deduct Rp.' . number_format($request->get('nominal'), 2) . ' from your Account',
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'error' => $ex->getMessage() . 'at ' . $ex->getLine(),
                'message' => $ex->getCode() . 'Withdrawal Failed',
            ]);
        }
    }

    public function deposit(Request $request)
    {
        try {
            $account = Account::findOrFail($request->get('account_id'));
            $newBalance = $account->balance + $request->get('nominal');
            $a = Transaction::create([
                'account_number' => $account->account_number,
                'customer_name' => $account->user->first_name . ' ' . $account->user->last_name,
                'type' => 'deposit',
                'amount' => $request->get('nominal')
            ]);
            $account->update([
                'balance' => $newBalance
            ]);
            return response()->json([
                'message' => 'Successfully added Rp.' . number_format($request->get('nominal'), 2) . ' to your Account',
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'message' => $ex->getMessage(),
            ]);
        }
    }

    public function updateBalance(Request $request)
    {
        $account = Account::findOrFail($request->get('account_id'));
        return response()->json([
            'balance' => 'Rp ' . number_format($account->balance, 2),
        ]);
    }

    public function transfer(Request $request)
    {
        try {
            if($request->get('sender_acc') == $request->get('recipient_acc')){
                return response()->json([
                    'message' => 'Sender Account cannot be the same as Recipient',
                ]);
            }
            $sender = Account::where('account_number', $request->get('sender_acc'))->get();
            $sender = $sender[0];
            $recipient = Account::where('account_number', $request->get('recipient_acc'))->get();
            if ($recipient->count() <= 0) {
                return response()->json([
                    'message' => 'Recipient account number not found, please check again',
                ]);
            }
            $recipient = $recipient[0];
            $nominal = $request->get('nominal');
            if($sender->balance < $nominal){
                return response()->json([
                    'message' => 'Insufficient Fund for Transfer',
                ]);
            }
            $senderBalance = $sender->balance - $nominal;
            $recipientbalance = $recipient->balance + $nominal;

            $sender->update([
                'balance' => $senderBalance,
            ]);

            Transaction::create([
                'account_number' => $sender->account_number,
                'customer_name' => $sender->user->first_name . ' ' . $sender->user->last_name,
                'type' => 'Transfer - Out',
                'amount' => $nominal
            ]);

            $recipient->update([
                'balance' => $recipientbalance,
            ]);

            Transaction::create([
                'account_number' => $recipient->account_number,
                'customer_name' => $recipient->user->first_name . ' ' . $recipient->user->last_name,
                'type' => 'Transfer - In',
                'amount' => $nominal
            ]);
            return response()->json([
                'message' => 'Transfer Success',
            ]);
        } catch (\Throwable $ex) {
            return response()->json([
                'error' => $ex->getMessage() . ' at ' . $ex->getFile() . ' ' . $ex->getLine(),
                'message' => $ex->getCode() . ' Transfer Failed',
                'data' => $recipient
            ]);
        }
    }

}
