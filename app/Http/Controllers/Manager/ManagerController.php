<?php

namespace App\Http\Controllers\Manager;

use App\Http\Resources\UserResource;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TransactionResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class ManagerController extends Controller
{
    public function index()
    {
        $transactions = Transaction::all();
        $customers = User::all();

        return view('manager.index', compact('transactions', 'customers'));
    }

    public function getRecords(Request $request)
    {
        $transactions = Transaction::all();

        return view('manager.index');

    }

    public function getTransaction(Request $request)
    {
        $transactions = Transaction::all();
        //return response()->json($transactions);
        return TransactionResource::collection($transactions);
    }

    public function getUsers(Request $request)
    {
        $users = User::with('accounts')->get();
        //return response()->json($transactions);
        return UserResource::collection($users);
    }
}
